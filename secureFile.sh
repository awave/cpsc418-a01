#!/usr/bin/env bash

# Author: Artem Golovin
# UID:    30018900
# Email:  artem.golovin@ucalgary.ca

if [ ! -d "./out" ]; then
  make build
fi

java -cp ./out secureFile $1 $2 $3
